import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import * as moment from 'moment';


@Component({
  selector: 'dashboard-cmp',
  moduleId: module.id,
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {

  chartTransaction: any;

  dataTransaction = [
    {
      id: 1,
      dateTime: new Date('2020-11-21 10:41'),
      transactionNumber: '04123921313',
      name: 'rico',
      nominal: 120000,
      accountType: 'Cash',
      isDelete: false,
    },
    {
      id: 2,
      dateTime: new Date('2020-11-20 18:12'),
      transactionNumber: '04123921314',
      name: 'ipoel',
      nominal: 50000,
      accountType: 'Cash Over',
      isDelete: false,
    }
  ];

  newDataTransactionList: any;

  sumTotalTransaction = 0;
  countTotalTransaction = 0;

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('transactionList'))) {
      this.newDataTransactionList = JSON.parse(localStorage.getItem('transactionList')).filter(item => { if (!item.isDelete) return true });
      this.sumTotalTransaction = this.newDataTransactionList.reduce((a, b) => a + parseInt(b.nominal), 0);
      this.countTotalTransaction = this.newDataTransactionList.length;
      console.log("data transaction ", this.dataTransaction);
      if (this.chartTransaction) {
        this.chartTransaction.destroy();
        this.getChartTransaction();
      } else {
        this.getChartTransaction();
      }
    } else {
      this.newDataTransactionList = [...this.dataTransaction];
      this.sumTotalTransaction = this.dataTransaction.reduce((a, b) => a + b.nominal, 0);
      this.countTotalTransaction = this.dataTransaction.length;
      if (this.chartTransaction) {
        this.chartTransaction.destroy();
        this.getChartTransaction();
      } else {
        this.getChartTransaction();
      }
      console.log("masuk else");
    }
  }

  getChartTransaction() {
    var speedCanvas = document.getElementById("speedChart");

    var dataFirst = {
      data: this.newDataTransactionList.map(data => data.nominal),
      fill: false,
      borderColor: '#fbc658',
      backgroundColor: 'transparent',
      pointBorderColor: '#fbc658',
      pointRadius: 4,
      pointHoverRadius: 4,
      pointBorderWidth: 8,
    };

    var speedData = {
      labels: this.newDataTransactionList.map(data => moment(data.dateTime).format('DD MMM YYYY hh:mm')),
      datasets: [dataFirst]
    };

    var chartOptions = {
      legend: {
        display: false,
        position: 'top'
      }
    };

    this.chartTransaction = new Chart(speedCanvas, {
      type: 'line',
      data: speedData,
      options: chartOptions
    });
  }
}
