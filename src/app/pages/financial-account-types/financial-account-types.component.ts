import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { FinancialTypeDialogComponent } from 'app/components/financial-type-dialog/financial-type-dialog.component';

@Component({
  selector: 'app-financial-account-types',
  templateUrl: './financial-account-types.component.html',
  styleUrls: ['./financial-account-types.component.css']
})
export class FinancialAccountTypesComponent implements OnInit {

  financialTypeList = [
    {
      id: 1,
      accountType: 'Cash',
      type: 'Asset',
      isDelete: false,
    },
    {
      id: 2,
      accountType: 'Cash Over',
      type: 'Revenue',
      isDelete: false,
    }
  ];

  constructor(
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    if (!JSON.parse(localStorage.getItem('accountTypeList'))) {
      localStorage.setItem('accountTypeList', JSON.stringify(this.financialTypeList));
    } else {
      this.financialTypeList = JSON.parse(localStorage.getItem('accountTypeList')).filter(data => {if(!data.isDelete) return true});
    }
  }

  addType() {
    const dialogRef = this.dialog.open(FinancialTypeDialogComponent, {
      width: '30vw',
      data: {
        origin: 'add',
        dataLen: this.financialTypeList.length,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.financialTypeList = JSON.parse(localStorage.getItem('accountTypeList')).filter(data => {if(!data.isDelete) return true});
      }
    });
  }

  editType(data) {
    const dialogRef = this.dialog.open(FinancialTypeDialogComponent, {
      width: '30vw',
      data: {
        origin: 'edit',
        data: data,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.financialTypeList = JSON.parse(localStorage.getItem('accountTypeList')).filter(data => {if(!data.isDelete) return true});
      }
    });
  }

  delete(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '30vw',
      data: {
        message: `Are you sure want to delete this '${data.accountType}'`,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // DELETE DATA
        this.financialTypeList.map(item => item.id === data.id ? item.isDelete = true : item.isDelete = false);
        // GET DATA
        this.financialTypeList = this.financialTypeList.filter(data => {if(!data.isDelete) return true});
        // SAVE DATA
        localStorage.setItem('accountTypeList', JSON.stringify(this.financialTypeList));
      }
    });
  }

}
