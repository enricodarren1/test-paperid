import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialAccountTypesComponent } from './financial-account-types.component';

describe('FinancialAccountTypesComponent', () => {
  let component: FinancialAccountTypesComponent;
  let fixture: ComponentFixture<FinancialAccountTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialAccountTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialAccountTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
