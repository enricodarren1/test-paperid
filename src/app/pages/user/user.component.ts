import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit{
    
    dataUserForm: FormGroup;
    dataUser: any;

    constructor(
        private fb: FormBuilder,
    ) {
        this.dataUser = JSON.parse(localStorage.getItem('dataUser'));
        console.log(this.dataUser);

        this.dataUserForm = this.fb.group({
            firstName: new FormControl(''),
            lastName: new FormControl(''),
            email: new FormControl(''),
            address: new FormControl(''),
            city: new FormControl(''),
            country: new FormControl(''),
            postalCode: new FormControl(''),
            description: new FormControl(''),
        });
        this.dataUserForm.patchValue(this.dataUser);
    }

    ngOnInit(){
    }

    onSubmit() {
        console.log("data user value ", this.dataUserForm.value);
        localStorage.setItem('dataUser', JSON.stringify(this.dataUserForm.value));
        this.dataUser = this.dataUserForm.value;
    }
}
