import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    })
  }

  onLogin() {
    console.log("form value ", this.loginForm.value);
    this.router.navigate(['/dashboard']);
    const body = {
      firstName: 'Dummy',
      lastName: 'Dummies',
      email: 'dummy@dummy.com',
      address: 'BSD City, Tangerang',
      city: 'BSD City',
      country: 'Indonesia',
      postalCode: '15154',
      description: 'Lorem ipsum'
    }
    localStorage.setItem('dataUser', JSON.stringify(body));
  }

}
