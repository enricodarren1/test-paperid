import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'app/components/confirm-dialog/confirm-dialog.component';
import { FinancialTransactionDialogComponent } from 'app/components/financial-transaction-dialog/financial-transaction-dialog.component';

@Component({
  selector: 'app-financial-transaction',
  templateUrl: './financial-transaction.component.html',
  styleUrls: ['./financial-transaction.component.css']
})
export class FinancialTransactionComponent implements OnInit {

  oldData: any
  dataTransactionList = [
    {
      id: 1,
      dateTime: new Date('2020-11-21 10:41'),
      transactionNumber: '04123921313',
      name: 'rico',
      nominal: 120000,
      accountType: 'Cash',
      isDelete: false,
    },
    {
      id: 2,
      dateTime: new Date('2020-11-20 18:12'),
      transactionNumber: '04123921314',
      name: 'ipoel',
      nominal: 50000,
      accountType: 'Cash Over',
      isDelete: false,
    }
  ];

  accountTypeList = [
    {
      id: 1,
      accountType: 'Cash',
      type: 'Asset',
      isDelete: false,
    },
    {
      id: 2,
      accountType: 'Cash Over',
      type: 'Revenue',
      isDelete: false,
    }
  ];

  filter = {
    dateTime: '',
    name: '',
    transactionNumber: '',
  }

  constructor(
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    if (!JSON.parse(localStorage.getItem('transactionList'))) {
      localStorage.setItem('transactionList', JSON.stringify(this.dataTransactionList));
    } else {
      this.dataTransactionList = JSON.parse(localStorage.getItem('transactionList')).filter(data => { if (!data.isDelete) return true });
      this.oldData = [...this.dataTransactionList];
    }

    if (JSON.parse(localStorage.getItem('accountTypeList'))) {
      this.accountTypeList = JSON.parse(localStorage.getItem('accountTypeList'));
    }
  }

  onSearch() {
    console.log("data transaction ", this.dataTransactionList);
    const tempData = [...this.dataTransactionList];
    if (this.filter.name && !this.filter.transactionNumber) {
      // FILTER BY NAME
      this.dataTransactionList = [...tempData.filter(item => item.name.includes(this.filter.name))];
    } else if (this.filter.transactionNumber && !this.filter.name) {
      // FILTER BY TRANSACTION NUMBER
      this.dataTransactionList = [...tempData.filter(item => item.transactionNumber.includes(this.filter.transactionNumber))];
    }  else if (this.filter.name && this.filter.transactionNumber) {
      // FILTER BY NAME && TRANSACTION NUMBER
      this.dataTransactionList = [...tempData.filter(item => item.name.includes(this.filter.name) && item.transactionNumber.includes(this.filter.transactionNumber))];
    } else {
      // REFRESH DATA
      this.dataTransactionList = this.oldData;
    }
  }

  add() {
    const dialogRef = this.dialog.open(FinancialTransactionDialogComponent, {
      data: {
        origin: 'add',
        dataLen: this.dataTransactionList.length,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.dataTransactionList = JSON.parse(localStorage.getItem('transactionList')).filter(data => { if (!data.isDelete) return true });
    });
  }

  edit(data) {
    const dialogRef = this.dialog.open(FinancialTransactionDialogComponent, {
      data: {
        origin: 'edit',
        data: data,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.dataTransactionList = JSON.parse(localStorage.getItem('transactionList')).filter(data => { if (!data.isDelete) return true });
    });
  }

  delete(data) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '30vw',
      data: {
        message: `Are you sure want to delete this '${data.name}'`,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // DELETE DATA
        this.dataTransactionList.map(item => item.id === data.id ? item.isDelete = true : item.isDelete = false);
        // GET DATA
        this.dataTransactionList = this.dataTransactionList.filter(data => { if (!data.isDelete) return true });
        // SAVE DATA
        localStorage.setItem('transactionList', JSON.stringify(this.dataTransactionList));
      }
    });
  }

}
