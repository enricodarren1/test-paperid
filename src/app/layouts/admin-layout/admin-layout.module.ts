import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { DashboardComponent }       from '../../pages/dashboard/dashboard.component';
import { UserComponent }            from '../../pages/user/user.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FinancialTransactionComponent } from 'app/pages/financial-transaction/financial-transaction.component';
import { FinancialAccountTypesComponent } from 'app/pages/financial-account-types/financial-account-types.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    NgbModule,
    SharedModule,
  ],
  declarations: [
    DashboardComponent,
    UserComponent,
    FinancialTransactionComponent,
    FinancialAccountTypesComponent,
  ]
})

export class AdminLayoutModule {}
