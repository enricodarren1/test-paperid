import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserComponent } from '../../pages/user/user.component';
import { FinancialTransactionComponent } from 'app/pages/financial-transaction/financial-transaction.component';
import { FinancialAccountTypesComponent } from 'app/pages/financial-account-types/financial-account-types.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'user',           component: UserComponent },
    { path: 'financial-transanction', component: FinancialTransactionComponent },
    { path: 'financial-account-types', component: FinancialAccountTypesComponent },
];
