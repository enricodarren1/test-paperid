import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-financial-transaction-dialog',
  templateUrl: './financial-transaction-dialog.component.html',
  styleUrls: ['./financial-transaction-dialog.component.css']
})
export class FinancialTransactionDialogComponent implements OnInit {

  financialTransctionForm: FormGroup;

  accountTypeList: any;

  maxDate = new Date();

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<FinancialTransactionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
    console.log("data ", this.data);

    this.financialTransctionForm = this.fb.group({
      transactionNumber: new FormControl({ value: '', disabled: this.data.origin === 'edit' ? true : false }, Validators.required),
      name: new FormControl('', Validators.required),
      dateTime: new FormControl('', Validators.required),
      accountType: new FormControl('', Validators.required),
      nominal: new FormControl('', Validators.required),
    });

    if (!JSON.parse(localStorage.getItem('accountTypeList'))) {
      this.accountTypeList = [
        {
          id: 1,
          accountType: 'Cash',
          type: 'Asset',
          isDelete: false,
        },
        {
          id: 2,
          accountType: 'Cash Over',
          type: 'Revenue',
          isDelete: false,
        }
      ];
    } else {
      this.accountTypeList = JSON.parse(localStorage.getItem('accountTypeList'));
    }

    if (this.data.origin === 'edit') {
      this.financialTransctionForm.patchValue(this.data.data);
      this.financialTransctionForm.controls.dateTime.setValue(new Date(this.data.data.dateTime));
    }
  }

  onSubmit() {
    console.log("financial transaction ", this.financialTransctionForm.value);
    // GET DATA
    let transactionList = JSON.parse(localStorage.getItem('transactionList'));

    if (this.data.origin === 'add') {
      const body = {
        id: this.data.dataLen + 1,
        transactionNumber: this.financialTransctionForm.value.transactionNumber,
        name: this.financialTransctionForm.value.name,
        dateTime: this.financialTransctionForm.value.dateTime,
        accountType: this.financialTransctionForm.value.accountType,
        nominal: this.financialTransctionForm.value.nominal,
        isDelete: false,
      }
      // ADD DATA
      transactionList.push(body);
    } else if (this.data.origin === 'edit') {
      // UPDATE DATA
      transactionList.map(item => {
        if (item.id === this.data.data.id) {
          item.name = this.financialTransctionForm.value.name;
          item.dateTime = this.financialTransctionForm.value.dateTime;
          item.accountType = this.financialTransctionForm.value.accountType;
          item.nominal = this.financialTransctionForm.value.nominal;
        }
      });
      console.log("transaction edit ", transactionList);
    }
    // SAVE DATA
    localStorage.setItem('transactionList', JSON.stringify(transactionList));
    this.dialogRef.close('success');
  }

}
