import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialTransactionDialogComponent } from './financial-transaction-dialog.component';

describe('FinancialTransactionDialogComponent', () => {
  let component: FinancialTransactionDialogComponent;
  let fixture: ComponentFixture<FinancialTransactionDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialTransactionDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialTransactionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
