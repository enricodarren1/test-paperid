import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-financial-type-dialog',
  templateUrl: './financial-type-dialog.component.html',
  styleUrls: ['./financial-type-dialog.component.css']
})
export class FinancialTypeDialogComponent implements OnInit {

  financialTypeForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<FinancialTypeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }

  ngOnInit(): void {
    this.financialTypeForm = this.fb.group({
      accountType: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
    });

    if (this.data.origin === 'edit') {
      this.financialTypeForm.patchValue(this.data.data);
    }
  }

  onSubmit() {
    // GET DATA
    let accountTypeList = JSON.parse(localStorage.getItem('accountTypeList'));

    if (this.data.origin === 'add') {
      const body = {
        id: this.data.dataLen + 1,
        accountType: this.financialTypeForm.value.accountType,
        type: this.financialTypeForm.value.type,
        isDelete: false,
      }
      // ADD DATA
      accountTypeList.push(body);
    } else if (this.data.origin === 'edit') {
      // UPDATE DATA
      accountTypeList.map(item => {
        if (item.id === this.data.data.id) {
          item.accountType = this.financialTypeForm.value.accountType;
          item.type = this.financialTypeForm.value.type;
        }
      });
    }
    // SAVE DATA
    localStorage.setItem('accountTypeList', JSON.stringify(accountTypeList));
    this.dialogRef.close('success');
  }

}
