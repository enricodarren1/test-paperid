import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinancialTypeDialogComponent } from './financial-type-dialog.component';

describe('FinancialTypeDialogComponent', () => {
  let component: FinancialTypeDialogComponent;
  let fixture: ComponentFixture<FinancialTypeDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinancialTypeDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinancialTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
